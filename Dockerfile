FROM php:5.6-fpm-alpine

ENV DD_LOGS_INJECTION=true
ENV DD_RUNTIME_METRICS_ENABLED=true
ENV DD_TRACE_VERSION 0.75.0
ENV ACCEPT_EULA=Y

# Install prerequisites required for tools and extensions installed later on.
RUN apk add --update bash gnupg libpng-dev libzip-dev su-exec unzip supervisor nginx unixodbc-dev

# Install datadog tracer
RUN curl -LO https://github.com/DataDog/dd-trace-php/releases/download/${DD_TRACE_VERSION}/datadog-php-tracer_${DD_TRACE_VERSION}_noarch.apk
RUN apk add datadog-php-tracer_${DD_TRACE_VERSION}_noarch.apk --allow-untrusted
RUN rm -f datadog-php-tracer_${DD_TRACE_VERSION}_noarch.apk›

# Retrieve the script used to install PHP extensions from the source container.
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/bin/install-php-extensions

# Install required PHP extensions and all their prerequisites available via apt.
RUN chmod uga+x /usr/bin/install-php-extensions \
    && sync \
    && install-php-extensions bcmath exif gd intl opcache pcntl redis zip mysqli mysql pdo_mysql pdo_odbc ldap soap imap mssql mcrypt imagick pdo_sqlsrv

# Downloading composer and marking it as executable.
RUN curl -o /usr/local/bin/composer https://getcomposer.org/composer-stable.phar \
    && chmod +x /usr/local/bin/composer

# Remove HTML folder inside /var/www
RUN rm -rf /var/www/html
RUN mkdir /var/www/public
COPY ./index.php /var/www/public/index.php

# Setting the work directory.
WORKDIR /var/www

# COPY NGINX CONFIG
COPY ./nginx/mime.types /etc/nginx/mime.types
COPY ./nginx/nginx.conf /etc/nginx/nginx.conf

# COPY PHP CONFIGS
COPY ./php/php.ini /usr/local/etc/php/php.ini
COPY ./php/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY ./php/www.conf /usr/local/etc/php-fpm.d/www.conf

# COPY CRONTAB CONFIGS
COPY ./crontabs/cron /etc/cron
RUN chmod 0644 /etc/cron && crontab /etc/cron

# COPY SUPERVISOR CONFIG
COPY ./supervisor/supervisord.conf /etc/supervisord.conf

# COPY SHELLSCRIPT
COPY ./scripts/init.sh /scripts/init.sh
RUN chmod +x /scripts/init.sh

EXPOSE 80